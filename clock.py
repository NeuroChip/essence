from apscheduler.schedulers.background import BackgroundScheduler


# The "apscheduler." prefix is hard coded
scheduler = BackgroundScheduler({
    'apscheduler.jobstores.mongo': {
        'type': 'mongodb',
        'url': os.environ.get('MONGOLAB_URI', 'mongodb://localhost:27017/scheduler')
    },
    'apscheduler.jobstores.default': {
        'type': 'sqlalchemy',
        'url': os.environ.get('DATABASE_URL', 'sqlite:///scheduler.sqlite')
    },
    'apscheduler.executors.default': {
        'class': 'apscheduler.executors.pool:ThreadPoolExecutor',
        'max_workers': '20'
    },
    'apscheduler.executors.processpool': {
        'type': 'processpool',
        'max_workers': '5'
    },
    'apscheduler.job_defaults.coalesce': 'false',
    'apscheduler.job_defaults.max_instances': '3',
    'apscheduler.timezone': 'UTC',
})

from apschedulerweb import start as apsweb_start

#def printer(sched):
#    print(sched)
#
#scheduler.add_interval_job(printer, args=['hello'], seconds=5)

apsweb_start(scheduler, users={'user':'pass'})

