>>> from pattern.search import search
>>> from pattern.en import parsetree
>>>  
>>> t = parsetree('big white rabbit')
>>> print t
>>> print
>>> print search('JJ', t) # all adjectives
>>> print search('NN', t) # all nouns
>>> print search('NP', t) # all noun phrases
 
[Sentence('big/JJ/B-NP/O white/JJ/I-NP/O rabbit/NN/I-NP/O')]

[Match(words=[Word(u'big/JJ')]), Match(words=[Word(u'white/JJ')])]
[Match(words=[Word(u'rabbit/NN')])]
[Match(words=[Word(u'big/JJ'), Word(u'white/JJ'), Word(u'rabbit/NN')])]

>>> from pattern.search import search, taxonomy
>>> from pattern.en import parsetree
>>>  
>>> for f in ('rose', 'lily', 'daisy', 'daffodil', 'begonia'):
>>>     taxonomy.append(f, type='flower')
>>> 
>>> t = parsetree('A field of white daffodils.', lemmata=True)
>>> print t
>>> print
>>> print search('FLOWER', t) 
 
[Sentence('A/DT/B-NP/O/a field/NN/I-NP/O/field of/IN/B-PP/B-PNP/of'
          'white/JJ/B-NP/I-PNP/white daffodils/NNS/I-NP/I-PNP/daffodil ././O/O/.')]
 
[Match(words=[Word(u'white/JJ'), Word(u'daffodils/NNS')])]

>>> from pattern.search import Pattern
>>> from pattern.en import parsetree
>>> 
>>> t = parsetree('Chuck Norris is cooler than Dolph Lundgren.', lemmata=True)
>>> p = Pattern.fromstring('{NP} be * than {NP}')
>>> m = p.match(t)
>>> print m.group(1)
>>> print m.group(2)
 
[Word(u'Chuck/NNP'), Word(u'Norris/NNP')]
[Word(u'Dolph/NNP'), Word(u'Lundgren/NNP')]

>>> from pattern.search import search
>>> from pattern.en import parsetree
>>>  
>>> t = parsetree('tasty cat food')
>>> print t 
>>> print
>>> print search('DT? RB? JJ? NN+', t)
 
[Sentence('tasty/JJ/B-NP/O cat/NN/I-NP/O food/NN/I-NP/O')]
 
[Match(words=[Word(u'tasty/JJ'), Word(u'cat/NN')]), Word(u'food/NN')])]

>>> from pattern.search import match
>>> from pattern.en import parsetree
>>>  
>>> t = parsetree('The turtle was faster than the hare.', lemmata=True)
>>> m = match('NP be ADJP|ADVP than NP', t)
>>>
>>> for w in m.words:
>>>    print w, '\t =>', m.constraint(w)
 
Word(u'The/DT')     => Constraint(chunks=['NP'])
Word(u'turtle/NN')  => Constraint(chunks=['NP'])
Word(u'was/VBD')    => Constraint(words=['be'])
Word(u'faster/RBR') => Constraint(chunks=['ADJP', 'ADVP'])
Word(u'than/IN')    => Constraint(words=['than'])
Word(u'the/DT')     => Constraint(chunks=['NP'])
Word(u'hare/NN')    => Constraint(chunks=['NP'])

