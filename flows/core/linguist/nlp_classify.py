>>> from pattern.vector import Document, NB
>>> from pattern.db import csv
>>>  
>>> nb = NB()
>>> for review, rating in csv('reviews.csv'):
>>>     v = Document(review, type=int(rating), stopwords=True) 
>>>     nb.train(v)
>>> 
>>> print nb.classes
>>> print nb.classify(Document('A good movie!'))
 
[0, 1, 2, 3, 4, 5]
4

>>> data = csv('reviews.csv')
>>> data = [(review, int(rating)) for review, rating in data]
>>> data = [Document(review, type=rating, stopwords=True) for review, rating in data]
>>> 
>>> nb = NB(train=data[:500])
>>> 
>>> accuracy, precision, recall, f1 = nb.test(data[500:])
>>> print accuracy
 
0.60

>>> from pattern.vector import NB, Document, kfoldcv
>>> from pattern.db import csv 
>>> 
>>> data = csv('reviews.csv') 
>>> data = [(review, int(rating) >= 3) for review, rating in data]
>>> data = [Document(review, type=rating, stopwords=True) for review, rating in data]
>>> 
>>> print kfoldcv(NB, data, folds=10)
 
(0.678, 0.563, 0.568, 0.565, 0.034)

>>> from pattern.vector import NB, kfoldcv, count
>>> from pattern.db import csv
>>> from pattern.en import parsetree
>>> 
>>> def v(review):
>>>     v = parsetree(review, lemmata=True)[0]
>>>     v = [w.lemma for w in v if w.tag.startswith(('JJ', 'NN', 'VB', '!'))]
>>>     v = count(v)
>>>     return v
>>> 
>>> data = csv('reviews.csv') 
>>> data = [(v(review), int(rating) >= 3) for review, rating in data]
>>> 
>>> print kfoldcv(NB, data)
 
(0.631, 0.588, 0.626, 0.606, 0.044)

>>> from pattern.vector import Model, Document, BINARY, NB, kfoldcv
>>> from pattern.db import csv
>>> 
>>> data = csv('reviews.csv') 
>>> data = [(review, int(rating) >= 3) for review, rating in data]
>>> data = [Document(review, type=rating, stopwords=True) for review, rating in data]
>>> 
>>> model = Model(documents=data, weight=TF)
>>> model = model.filter(features=model.feature_selection(top=1000))
>>> 
>>> print kfoldcv(NB, model)
 
(0.769, 0.689, 0.639, 0.662, 0.043)

