# -*- coding: utf-8 -*-

from octopus.shortcuts import *

#****************************************************************************************

class ConfreaksSpider(CrawlSpider):
    name = "confreaks"
    allowed_domains = [
        'confreaks.com',
        'www.confreaks.com',
    ]

    start_urls = (
        'http://www.confreaks.com/events',
        'http://www.confreaks.com/presenters',
    )

    rules = (
        Rule(LinkExtractor(allow=('events', )),                                         follow=True),
        Rule(LinkExtractor(allow=('presenters', )),                                     follow=True),

        Rule(LinkExtractor(allow=('events\/(.*)', )),       callback='parse_event',     follow=True),
        Rule(LinkExtractor(allow=('presenters\/(.*)', )),   callback='parse_presenter', follow=True),
        Rule(LinkExtractor(allow=('videos\/(.*)', )),       callback='parse_video',     follow=True),
    )

    #***************************************************************************

    def wrap_event(self, event):
        return dict(
            slug         = event.xpath('./@href').extract()[0].replace('/events/',''),
            fullname     = cleanup(event.xpath('./text()'), sep=None, trim=True, inline=True),
        )

    def parse_event(self, response):
        pass

    #***************************************************************************

    def wrap_presenter(self, person):
        uid,slug = person.xpath('./@href').extract()[0].replace('/presenters/','').split('-', 1)

        return dict(
            uid          = long(uid),
            slug         = slug,
            fullname     = cleanup(person.xpath('./text()'), sep=None, trim=True, inline=True),
        )

    def parse_presenter(self, response):
        pass

    #***************************************************************************

    def parse_video(self, response):
        obj = WebTalk()

        obj['site']        = self.name

        obj['slug'], obj['uid'] = urlparse(response.url).path.replace('/videos/', '').split('-', 1)

        obj['title']       = cleanup(response.xpath("//*[@class='video-title']//text()"), sep=None, trim=True, inline=True)
        obj['summary']     = cleanup(response.xpath("//*[@class='video-note']//text()"), sep=None, trim=True, inline=True)
        obj['description'] = cleanup(response.xpath("//*[@class='video-abstract']//text()"), sep=None, trim=True, inline=True)

        flags = [
            x.strip()
            for x in response.xpath("//*[@class='video-rating']/strong/text()").extract()
        ]

        obj['when']        = cleanup(response.xpath("//*[@class='video-posted-on']/strong/text()"), sep=None, trim=True, inline=True)
        obj['rating']      = flags[0]
        obj['views']       = long(flags[1])
        obj['link']        = urlparse(response.url).path

        obj['event']       = self.wrap_event(response.xpath("//*[@class='center']/h3/a"))
        obj['presenters']  = [
            self.wrap_presenter(entry)
            for entry in response.xpath("//*[@class='video-presenters']/a")
        ]
        obj['license']     = cleanup(response.xpath("//*[@class='video-license']/div//text()"), sep=None, trim=True, inline=True)

        obj['provider']    = 'youtube'
        obj['narrow']      = cleanup(response.xpath("//*[@class='video-frame']/iframe/@src"), sep=None, trim=True, inline=True).replace('http://www.youtube.com/embed/','')

        obj['media'] = {
            'cover': cleanup(response.xpath("//*[@class='video-thumbnail']/img/@src"), sep=None, trim=True, inline=True),
        }

        yield obj

    def parse_related(self, response):
        for mdl in response.xpath('//table//tr[@data-ref]'):
            srv = PhysicalServer()

            srv['link']        = response.url
            srv['brand']       = 'kimsufi'
            srv['narrow']      = cleanup(mdl.xpath('./td[1]//text()'), sep=None, trim=True, inline=True)

            srv['specs_cpu']   = {
                'model': cleanup(mdl.xpath('./td[2]//text()'), sep='', trim=True, inline=True),
                #'specs': [
                #    unicode(x).strip()
                #    for x in cleanup(mdl.xpath('./td[4]//text()'), sep=None, trim=True).split('/')
                #],
                'freq': cleanup(mdl.xpath('./td[5]//text()'), sep=None, trim=True, inline=True),
            }

            srv['specs_ram']   = {
                'raw':       cleanup(mdl.xpath('./td[6]//text()'), sep=str, trim=True, inline=True),
            }

            srv['specs_hdd']   = {
                'raw':       cleanup(mdl.xpath('./td[7]//text()'), sep=str, trim=True, inline=True),
            }

            srv['specs_net']   = {
                'bandwidth': cleanup(mdl.xpath('./td[8]//text()'), sep=str, trim=True, inline=True),
                'ipv4':      '/32',
                'ipv6':      cleanup(mdl.xpath('./td[9]//text()'), sep=None, trim=True, inline=True),
            }

            srv['pricing'] = {
                'ontime':  cleanup(mdl.xpath("//div[@class='section2']/p[2]/span[1]/span[5]/span[1]//text()"), sep=None, trim=True, inline=True),
                'monthly': cleanup(mdl.xpath('./td[10]/span[5]/span[1]//text()'), sep=None, trim=True, inline=True),
            }

            srv['pricing']['ontime']     += cleanup(mdl.xpath("//div[@class='section2']/p[2]/span[1]/span[5]/span[1]/text()"), sep=None, trim=True, inline=True)
            srv['pricing']['monthly']    += cleanup(mdl.xpath('./td[10]/span[5]/span[1]/text()'), sep=None, trim=True, inline=True)

            srv['pricing']['ontime']     += ' MAD'
            srv['pricing']['monthly']    += ' MAD'

            yield srv.cleanup()
