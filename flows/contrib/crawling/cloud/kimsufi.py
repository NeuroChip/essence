# -*- coding: utf-8 -*-

from octopus.shortcuts import *

#****************************************************************************************

class KimsufiSpider(CrawlSpider):
    name = "kimsufi"
    allowed_domains = [
        'kimsufi.com',
        'www.kimsufi.com',
    ]
    
    start_urls = (
        'https://www.kimsufi.com/ma/index.xml',
    )
    
    def parse(self, response):
        for mdl in response.xpath('//table//tr[@data-ref]'):
            srv = PhysicalServer()
            
            srv['link']        = response.url
            srv['brand']       = 'kimsufi'
            srv['narrow']      = cleanup(mdl.xpath('./td[1]//text()'), sep=None, trim=True, inline=True)
            
            srv['specs_cpu']   = {
                'model': cleanup(mdl.xpath('./td[2]//text()'), sep='', trim=True, inline=True),
                #'specs': [
                #    unicode(x).strip()
                #    for x in cleanup(mdl.xpath('./td[4]//text()'), sep=None, trim=True).split('/')
                #],
                'freq': cleanup(mdl.xpath('./td[5]//text()'), sep=None, trim=True, inline=True),
            }
            
            srv['specs_ram']   = {
                'raw':       cleanup(mdl.xpath('./td[6]//text()'), sep=str, trim=True, inline=True),
            }
            
            srv['specs_hdd']   = {
                'raw':       cleanup(mdl.xpath('./td[7]//text()'), sep=str, trim=True, inline=True),
            }
            
            srv['specs_net']   = {
                'bandwidth': cleanup(mdl.xpath('./td[8]//text()'), sep=str, trim=True, inline=True),
                'ipv4':      '/32',
                'ipv6':      cleanup(mdl.xpath('./td[9]//text()'), sep=None, trim=True, inline=True),
            }
            
            srv['pricing'] = {
                'ontime':  cleanup(mdl.xpath("//div[@class='section2']/p[2]/span[1]/span[5]/span[1]//text()"), sep=None, trim=True, inline=True),
                'monthly': cleanup(mdl.xpath('./td[10]/span[5]/span[1]//text()'), sep=None, trim=True, inline=True),
            }
            
            srv['pricing']['ontime']     += cleanup(mdl.xpath("//div[@class='section2']/p[2]/span[1]/span[5]/span[1]/text()"), sep=None, trim=True, inline=True)
            srv['pricing']['monthly']    += cleanup(mdl.xpath('./td[10]/span[5]/span[1]/text()'), sep=None, trim=True, inline=True)
            
            srv['pricing']['ontime']     += ' MAD'
            srv['pricing']['monthly']    += ' MAD'
            
            yield srv.cleanup()

